# OpenFlexure LED board

An illumination board for the [Openflexure Microscope](https://openflexure.org/projects/microscope/). Available as part of the [Illumination kit](https://taulab.eu/product/openflexure-microscope-illumination-kit). Based on the [original constant current illumination module](https://gitlab.com/openflexure/openflexure-constant-current-illumination).

This is a basic LED carrier board with current regulation, intended to be used with the Sangaboard v0.5's constant current driver or other electronics capable of controlling the current.

Connecting this to 5V directly will kill the LED!

If you wish to use this without the Sangaboard, the easiest option is to add a series resistor and connect it to a 5V supply. A (30-100) Ohm resistor should work well depending on the desired brightness. The LED can also be driven from a 3.3V source, just adjust the resistor value depending on the desired current.

For use without a Sangaboard, you can use [the original constant current illumination board](https://gitlab.com/openflexure/openflexure-constant-current-illumination) instead as it features a 20mA constant current driver.

## Choice of LED
The LED on this board is BXEN-50S-11M-3C ([datasheet](https://www.bridgelux.com/sies/default/files/resource_media/DS312%20SMD%202835%20Thrive97%200.5W%203V%20150mA%2011M-3C%20Rev%20C%2020220714.pdf)). This cold white LED was chosen for its flat spectrum (typical white LEDs have a more spiky spectrum with a large dip around 480nm). The LED is rated to 150mA drive current, but with the lack of airflow it would likely melt the condenser housing at that current. The Sangaboard v5 will drive the LED with approximately 30mA by default (adjustable between 0 and 100 mA)
