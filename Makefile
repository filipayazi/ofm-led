all: panel/ofm_cc_illumination_panel.kicad_pcb panel/jlcpcb/pos.csv


panel/jlcpcb/pos.csv: ofm_cc_illumination.kicad_sch panel/ofm_cc_illumination_panel.kicad_pcb
	kikit fab jlcpcb --no-drc --assembly --schematic ofm_cc_illumination.kicad_sch panel/ofm_cc_illumination_panel.kicad_pcb panel/fab

panel/ofm_cc_illumination_panel.kicad_pcb: ofm_cc_illumination.kicad_pcb Makefile
	kikit panelize --layout 'grid; rows: 5; cols: 5; space: 2mm' --tabs 'fixed; hwidth: 2mm; vwidth: 2mm' --cuts 'mousebites; drill:0.5mm; spacing:0.9mm; offset:0.25mm' --post 'millradius: 1mm' --framing railslr --fiducials '4fid; voffset: 8mm; hoffset:3mm;' --tooling "4hole; size: 1.5mm; voffset: 3mm; hoffset: 3mm;" --text 'simple; text: JLCJLCJLCJLC; vjustify: center; anchor: ml; orientation: 90deg; hoffset: 2mm;' $< $@

clean:
	rm -f panel/ofm_cc_illumination_panel.kicad_pcb

.PHONY: all clean
